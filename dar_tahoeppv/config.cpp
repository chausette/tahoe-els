////////////////////////////////////////////////////////////////////
//DeRap: Produced from mikero's Dos Tools Dll version 4.97
//Tue Jun 30 16:02:00 2015 : Source 'file' date Tue Jun 30 16:02:00 2015
//http://dev-heaven.net/projects/list_files/mikero-pbodll
////////////////////////////////////////////////////////////////////

#define _ARMA_

//Class dar_tahoeppv : config.bin{
class DefaultEventhandlers;
class CfgVehicleClasses
{
	class DARPol_Vehicles
	{
		displayName = "DAR ILPD Vehicles";
	};
};
class CfgSounds
{
	sounds[] = {"Siren1","Siren2","Siren3","Airhorn","Whelen_Howler","Whelen_Manual","Whelen_Takedown"};
	class Siren1
	{
		name = "Siren1";
		sound[] = {"DAR_TahoePPV\Sound\siren1.wav",1,1};
		titles[] = {};
	};
	class Siren2
	{
		name = "Siren2";
		sound[] = {"DAR_TahoePPV\Sound\siren2.wav",1,1};
		titles[] = {};
	};
	class Siren3
	{
		name = "Siren3";
		sound[] = {"DAR_TahoePPV\Sound\siren3.wav",1,1};
		titles[] = {};
	};
	class Airhorn
	{
		sound[] = {"\DAR_TahoePPV\Sound\Airhorn.wav",0.95,1};
		name = "Airhorn";
		titles[] = {};
	};
	class Whelen_Howler
	{
		sound[] = {"DAR_TahoePPV\Sound\Whelen_Howler.wav",0.95,1};
		name = "Whelen Howler";
		titles[] = {};
	};
	class Whelen_Manual
	{
		sound[] = {"DAR_TahoePPV\Sound\Whelen_Manual.wav",0.95,1};
		name = "Whelen Manual";
		titles[] = {};
	};
	class Whelen_Takedown
	{
		sound[] = {"DAR_TahoePPV\Sound\Whelen_Takedown.wav",0.95,1};
		name = "Whelen Takedown";
		titles[] = {};
	};
};
class CfgPatches
{
	class DAR_Tahoe_F
	{
		units[] = {"DAR_TahoePPV"};
		requiredVersion = 0.1;
		requiredAddons[] = {"A3_Soft_F"};
	};
};
class cfgWeapons
{
	class SportCarHorn;
	class Airhorn: SportCarHorn
	{
		displayname = "Airhorn";
		reloadTime = 0.891;
		scope = 2;
		drySound[] = {"DAR_TahoePPV\Sound\Airhorn.wav",1,1};
	};
	class Whelen_Howler: SportCarHorn
	{
		displayname = "Whelen Howler";
		reloadTime = 4;
		drySound[] = {"DAR_TahoePPV\Sound\Whelen_Howler.wav",1,1};
		scope = 2;
	};
	class Whelen_Manual: SportCarHorn
	{
		displayname = "Whelen Manual";
		reloadTime = 4;
		drySound[] = {"DAR_TahoePPV\Sound\Whelen_Manual.wav",1,1};
		scope = 2;
	};
	class Whelen_Takedown: SportCarHorn
	{
		displayname = "Whelen Takedown";
		reloadTime = 4;
		drySound[] = {"DAR_TahoePPV\Sound\Whelen_Takedown.wav",1,1};
		scope = 2;
	};
};
class CfgVehicles
{
	class LandVehicle;
	class Car: LandVehicle
	{
		class HitPoints;
		class NewTurret;
	};
	class Car_F: Car
	{
		class HitPoints
		{
			class HitLFWheel;
			class HitLF2Wheel;
			class HitRFWheel;
			class HitRF2Wheel;
			class HitBody;
			class HitGlass1;
			class HitGlass2;
			class HitGlass3;
			class HitGlass4;
		};
		class EventHandlers;
		class AnimationSources;
	};
	class Offroad_01_base_F: Car_F{};
	class C_Offroad_01_F: Offroad_01_base_F{};
	class TCG_Car_Base: C_Offroad_01_F{};
	class DAR_TahoePolBase: TCG_Car_Base
	{
		mapSize = 3.56;
		author = "Ported by RichardsD, from BxBugs123";
		_generalMacro = "";
		scope = 0;
		displayName = "2007 Chevrolet Tahoe PPV";
		vehicleclass = "DARPol_Vehicles";
		class Library
		{
			libTextDesc = "";
		};
		hiddenSelections[] = {"jump","lb-left-front-corner","lb-left-takedown","lb-front-blue-1","lb-front-blue-2","lb-front-red-2","lb-front-red-1","lb-right-takedown","lb-right-front-corner","lb-right-back-corner","lb-back-red-1","lb-back-red-2","lb-back-red-3","lb-back-blue-3","lb-back-blue-2","lb-back-blue-1","lb-left-back-corner","lb-back-yellow-1","lb-back-yellow-2","lb-back-yellow-3","lb-back-yellow-4","lb-back-yellow-5","lb-back-yellow-6","lb-left-alley","lb-right-alley","lb-ion-blue","lb-ion-red","radar_patrol_c","radar_patrol_d","radar_patrol_u","radar_fast_c","radar_fast_d","radar_fast_u","radar_target_c","radar_target_d","radar_target_u"};
		hiddenSelectionsTextures[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
		picture = "\DAR_TahoePPV\Data\UI\TahoeIcon.paa";
		Icon = "\DAR_TahoePPV\Data\UI\map_tahoe.paa";
		terrainCoef = 3.5;
		turnCoef = 2.5;
		precision = 10;
		brakeDistance = 3;
		acceleration = 15;
		fireResistance = 5;
		armor = 70;
		cost = 50000;
		transportMaxBackpacks = 10;
		transportSoldier = 3;
		wheelDamageRadiusCoef = 0.9;
		wheelDestroyRadiusCoef = 0.4;
		maxFordingDepth = 0.5;
		waterResistance = 1;
		crewCrashProtection = 0.8;
		class Exhausts
		{
			class Exhaust1
			{
				position = "exhaust1";
				direction = "exhaust1_dir";
				effect = "ExhaustEffectOffroad";
			};
		};
		ejectDeadCargo = 1;
		ejectDeadDriver = 1;
		class Turrets{};
		showNVGCargo[] = {0};
		showNVGDriver = 0;
		hideWeaponsDriver = 0;
		hideWeaponsCargo = 0;
		weapons[] = {"Airhorn","Whelen_Howler","Whelen_Manual","Whelen_Takedown"};
		driverAction = "driver_offroad01";
		getInAction = "GetInOffroad";
		getOutAction = "GetOutLow";
		cargoAction[] = {"passenger_low01","passenger_generic01_leanright","passenger_generic01_foldhands"};
		cargoGetInAction[] = {"GetInLow"};
		cargoGetOutAction[] = {"GetOutLow"};
		attenuationEffectType = "CarAttenuation";
		soundGetIn[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_door",0.562341,1};
		soundGetOut[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_door",0.562341,1,40};
		soundDammage[] = {"",0.562341,1};
		soundEngineOnInt[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_start_int.wav",0.398107,1};
		soundEngineOnExt[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_start_ext.wav",0.446684,1,200};
		soundEngineOffInt[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_stop_int.wav",0.398107,1};
		soundEngineOffExt[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_stop_ext.wav",0.446684,1,200};
		buildCrash0[] = {"A3\Sounds_F\vehicles\soft\noises\crash_building_01",0.707946,1,200};
		buildCrash1[] = {"A3\Sounds_F\vehicles\soft\noises\crash_building_02",0.707946,1,200};
		buildCrash2[] = {"A3\Sounds_F\vehicles\soft\noises\crash_building_03",0.707946,1,200};
		buildCrash3[] = {"A3\Sounds_F\vehicles\soft\noises\crash_building_04",0.707946,1,200};
		soundBuildingCrash[] = {"buildCrash0",0.25,"buildCrash1",0.25,"buildCrash2",0.25,"buildCrash3",0.25};
		WoodCrash0[] = {"A3\Sounds_F\vehicles\soft\noises\crash_mix_wood_01",0.707946,1,200};
		WoodCrash1[] = {"A3\Sounds_F\vehicles\soft\noises\crash_mix_wood_02",0.707946,1,200};
		WoodCrash2[] = {"A3\Sounds_F\vehicles\soft\noises\crash_mix_wood_03",0.707946,1,200};
		WoodCrash3[] = {"A3\Sounds_F\vehicles\soft\noises\crash_mix_wood_04",0.707946,1,200};
		WoodCrash4[] = {"A3\Sounds_F\vehicles\soft\noises\crash_mix_wood_05",0.707946,1,200};
		WoodCrash5[] = {"A3\Sounds_F\vehicles\soft\noises\crash_mix_wood_06",0.707946,1,200};
		soundWoodCrash[] = {"woodCrash0",0.166,"woodCrash1",0.166,"woodCrash2",0.166,"woodCrash3",0.166,"woodCrash4",0.166,"woodCrash5",0.166};
		ArmorCrash0[] = {"A3\Sounds_F\vehicles\soft\noises\crash_vehicle_01",0.707946,1,200};
		ArmorCrash1[] = {"A3\Sounds_F\vehicles\soft\noises\crash_vehicle_02",0.707946,1,200};
		ArmorCrash2[] = {"A3\Sounds_F\vehicles\soft\noises\crash_vehicle_03",0.707946,1,200};
		ArmorCrash3[] = {"A3\Sounds_F\vehicles\soft\noises\crash_vehicle_04",0.707946,1,200};
		soundArmorCrash[] = {"ArmorCrash0",0.25,"ArmorCrash1",0.25,"ArmorCrash2",0.25,"ArmorCrash3",0.25};
		class Sounds
		{
			class Idle_ext
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_idle_ext.wav",0.601187,1,200};
				frequency = "0.9 + ((rpm/ 6000) factor[(400/ 6000),(1100/ 6000)])*0.2";
				volume = "engineOn*camPos*(((rpm/ 6000) factor[(200/ 6000),(600/ 6000)]) * ((rpm/ 6000) factor[(1100/ 6000),(800/ 6000)]))";
			};
			class Engine
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_01_ext.wav",0.562341,1,200};
				frequency = "0.8 + ((rpm/ 6000) factor[(810/ 6000),(2000/ 6000)])*0.2";
				volume = "engineOn*camPos*(((rpm/ 6000) factor[(820/ 6000),(1100/ 6000)]) * ((rpm/ 6000) factor[(2000/ 6000),(1350/ 6000)]))";
			};
			class Engine1_ext
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_02_ext.wav",0.630957,1,210};
				frequency = "0.8 + ((rpm/ 6000) factor[(1300/ 6000),(2700/ 6000)])*0.2";
				volume = "engineOn*camPos*(((rpm/ 6000) factor[(1300/ 6000),(2000/ 6000)]) * ((rpm/ 6000) factor[(2700/ 6000),(2150/ 6000)]))";
			};
			class Engine2_ext
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_03_ext.wav",0.70794576,1,220};
				frequency = "0.8 + ((rpm/ 6000) factor[(2100/ 6000),(3500/ 6000)])*0.2";
				volume = "engineOn*camPos*(((rpm/ 6000) factor[(2150/ 6000),(2650/ 6000)]) * ((rpm/ 6000) factor[(3550/ 6000),(2900/ 6000)]))";
			};
			class Engine3_ext
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_04_ext.wav",0.7943282,1,230};
				frequency = "0.8 + ((rpm/ 6000) factor[(2900/ 6000),(4150/ 6000)])*0.2";
				volume = "engineOn*camPos*(((rpm/ 6000) factor[(2900/ 6000),(3500/ 6000)]) * ((rpm/ 6000) factor[(4180/ 6000),(3700/ 6000)]))";
			};
			class Engine4_ext
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_05_ext.wav",1,1,240};
				frequency = "0.8 + ((rpm/ 6000) factor[(3700/ 6000),(5200/ 6000)]) *0.2";
				volume = "engineOn*camPos*(((rpm/ 6000) factor[(3700/ 6000),(4100/ 6000)]) * ((rpm/ 6000) factor[(5200/ 6000),(4500/ 6000)]))";
			};
			class Engine5_ext
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_06_ext.wav",1.2589254,1,250};
				frequency = "0.95 + ((rpm/ 6000) factor[(4500/ 6000),(6000/ 6000)])*0.2";
				volume = "engineOn*camPos*((rpm/ 6000) factor[(4500/ 6000),(5600/ 6000)])";
			};
			class IdleThrust
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_idle",0.298107,1,280};
				frequency = "0.9 + ((rpm/ 6000) factor[(400/ 6000),(1100/ 6000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(200/ 6000),(600/ 6000)]) * ((rpm/ 6000) factor[(1100/ 6000),(800/ 6000)]))";
			};
			class EngineThrust
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_03",0.346684,1,300};
				frequency = "0.8 + ((rpm/ 6000) factor[(810/ 6000),(2000/ 6000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(820/ 6000),(1100/ 6000)]) * ((rpm/ 6000) factor[(2000/ 6000),(1350/ 6000)]))";
			};
			class Engine1_Thrust_ext
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_05",0.401187,1,310};
				frequency = "0.8 + ((rpm/ 6000) factor[(1300/ 6000),(2700/ 6000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(1300/ 6000),(2000/ 6000)]) * ((rpm/ 6000) factor[(2700/ 6000),(2150/ 6000)]))";
			};
			class Engine2_Thrust_ext
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_07",0.462341,1,320};
				frequency = "0.8 + ((rpm/ 6000) factor[(2100/ 6000),(3500/ 6000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(2150/ 6000),(2650/ 6000)]) * ((rpm/ 6000) factor[(3550/ 6000),(2900/ 6000)]))";
			};
			class Engine3_Thrust_ext
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_09",0.530957,1,330};
				frequency = "0.8 + ((rpm/ 6000) factor[(2900/ 6000),(4150/ 6000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(2900/ 6000),(3500/ 6000)]) * ((rpm/ 6000) factor[(4180/ 6000),(3700/ 6000)]))";
			};
			class Engine4_Thrust_ext
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_11",0.462341,1,340};
				frequency = "0.8 + ((rpm/ 6000) factor[(3700/ 6000),(5200/ 6000)]) *0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(3700/ 6000),(4100/ 6000)]) * ((rpm/ 6000) factor[(5200/ 6000),(4500/ 6000)]))";
			};
			class Engine5_Thrust_ext
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_13",0.401187,1,350};
				frequency = "0.9 + ((rpm/ 6000) factor[(4500/ 6000),(6000/ 6000)])*0.2";
				volume = "engineOn*camPos*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/ 6000) factor[(4500/ 6000),(5600/ 6000)])";
			};
			class Idle_int
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_idle_int.wav",0.31622776,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(400/ 6000),(1100/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/ 6000) factor[(200/ 6000),(600/ 6000)]) * ((rpm/ 6000) factor[(1100/ 6000),(800/ 6000)]))";
			};
			class Engine_int
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_01_int.wav",0.3548134,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(810/ 6000),(2000/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/ 6000) factor[(820/ 6000),(1100/ 6000)]) * ((rpm/ 6000) factor[(2000/ 6000),(1350/ 6000)]))";
			};
			class Engine1_int
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_02_int.wav",0.398107,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(1300/ 6000),(2700/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/ 6000) factor[(1300/ 6000),(2000/ 6000)]) * ((rpm/ 6000) factor[(2700/ 6000),(2150/ 6000)]))";
			};
			class Engine2_int
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_03_int.wav",0.4466836,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(2100/ 6000),(3500/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/ 6000) factor[(2150/ 6000),(2650/ 6000)]) * ((rpm/ 6000) factor[(3550/ 6000),(2900/ 6000)]))";
			};
			class Engine3_int
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_04_int.wav",0.5011872,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(2900/ 6000),(4150/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(((rpm/ 6000) factor[(2900/ 6000),(3500/ 6000)]) * ((rpm/ 6000) factor[(4180/ 6000),(3700/ 6000)]))";
			};
			class Engine4_int
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_05_int.wav",0.562341,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(3700/ 6000),(5200/ 6000)]) *0.2";
				volume = "engineOn*(1-camPos)*(((rpm/ 6000) factor[(3700/ 6000),(4100/ 6000)]) * ((rpm/ 6000) factor[(5200/ 6000),(4500/ 6000)]))";
			};
			class Engine5_int
			{
				sound[] = {"DAR_TahoePPV\Sound\Engine\TAHOE_eng_06_int.wav",0.630957,1};
				frequency = "0.95 + ((rpm/ 6000) factor[(4500/ 6000),(6000/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*((rpm/ 6000) factor[(4500/ 6000),(5600/ 6000)])";
			};
			class IdleThrust_int
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_idle_int",0.3548134,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(400/ 6000),(1100/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(200/ 6000),(600/ 6000)]) * ((rpm/ 6000) factor[(1100/ 6000),(800/ 6000)]))";
			};
			class EngineThrust_int
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_03_int",0.398107,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(810/ 6000),(2000/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(820/ 6000),(1100/ 6000)]) * ((rpm/ 6000) factor[(2000/ 6000),(1350/ 6000)]))";
			};
			class Engine1_Thrust_int
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_05_int",0.4466836,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(1300/ 6000),(2700/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(1300/ 6000),(2000/ 6000)]) * ((rpm/ 6000) factor[(2700/ 6000),(2150/ 6000)]))";
			};
			class Engine2_Thrust_int
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_07_int",0.5011872,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(2100/ 6000),(3500/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(2150/ 6000),(2650/ 6000)]) * ((rpm/ 6000) factor[(3550/ 6000),(2900/ 6000)]))";
			};
			class Engine3_Thrust_int
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_09_int",0.5011872,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(2900/ 6000),(4150/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(2900/ 6000),(3500/ 6000)]) * ((rpm/ 6000) factor[(4180/ 6000),(3700/ 6000)]))";
			};
			class Engine4_Thrust_int
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_11_int",0.562341,1};
				frequency = "0.8 + ((rpm/ 6000) factor[(3700/ 6000),(5200/ 6000)]) *0.3";
				volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*(((rpm/ 6000) factor[(3700/ 6000),(4100/ 6000)]) * ((rpm/ 6000) factor[(5200/ 6000),(4500/ 6000)]))";
			};
			class Engine5_Thrust_int
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\SUV_01\SUV_01_exhaust_13_int",0.630957,1};
				frequency = "0.9 + ((rpm/ 6000) factor[(4500/ 6000),(6000/ 6000)])*0.2";
				volume = "engineOn*(1-camPos)*(0.4+(0.6*(thrust factor[0.1,1])))*((rpm/ 6000) factor[(4500/ 6000),(5600/ 6000)])";
			};
			class Movement
			{
				sound = "soundEnviron";
				frequency = "1";
				volume = "0";
			};
			class TiresRockOut
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_dirt_soft_1",0.5011872,1,60};
				frequency = "1";
				volume = "camPos*rock*(speed factor[2, 20])";
			};
			class TiresSandOut
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext-tires-sand1",0.5011872,1,60};
				frequency = "1";
				volume = "camPos*sand*(speed factor[2, 20])";
			};
			class TiresGrassOut
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_dirt_soft_2",0.5011872,1,60};
				frequency = "1";
				volume = "camPos*grass*(speed factor[2, 20])";
			};
			class TiresMudOut
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext-tires-mud2",0.5011872,1,60};
				frequency = "1";
				volume = "camPos*mud*(speed factor[2, 20])";
			};
			class TiresGravelOut
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_gravel_1",0.5011872,1,60};
				frequency = "1";
				volume = "camPos*gravel*(speed factor[2, 20])";
			};
			class TiresAsphaltOut
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\ext_tires_asfalt_2",0.5011872,1,60};
				frequency = "1";
				volume = "camPos*asphalt*(speed factor[2, 20])";
			};
			class NoiseOut
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\noise_ext_car_3",0.398107,1,90};
				frequency = "1";
				volume = "camPos*(damper0 max 0.02)*(speed factor[0, 8])";
			};
			class TiresRockIn
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_dirt_soft_1",0.5011872,1};
				frequency = "1";
				volume = "(1-camPos)*rock*(speed factor[2, 20])";
			};
			class TiresSandIn
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int-tires-sand2",0.5011872,1};
				frequency = "1";
				volume = "(1-camPos)*sand*(speed factor[2, 20])";
			};
			class TiresGrassIn
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_dirt_soft_2",0.5011872,1};
				frequency = "1";
				volume = "(1-camPos)*grass*(speed factor[2, 20])";
			};
			class TiresMudIn
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int-tires-mud2",0.5011872,1};
				frequency = "1";
				volume = "(1-camPos)*mud*(speed factor[2, 20])";
			};
			class TiresGravelIn
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_gravel_1",0.5011872,1};
				frequency = "1";
				volume = "(1-camPos)*gravel*(speed factor[2, 20])";
			};
			class TiresAsphaltIn
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\tires\int_tires_asfalt_2",0.5011872,1};
				frequency = "1";
				volume = "(1-camPos)*asphalt*(speed factor[2, 20])";
			};
			class NoiseIn
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\noise_int_car_3",0.25118864,1};
				frequency = "1";
				volume = "(damper0 max 0.1)*(speed factor[0, 8])*(1-camPos)";
			};
			class breaking_ext_road
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_04",0.70794576,1,80};
				frequency = 1;
				volume = "engineOn*camPos*asphalt*(LongSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
			};
			class acceleration_ext_road
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02",0.70794576,1,80};
				frequency = 1;
				volume = "engineOn*camPos*asphalt*(LongSlipDrive Factor[0.1, 0.4])*(Speed Factor[15, 2])";
			};
			class turn_left_ext_road
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02",0.70794576,1,80};
				frequency = 1;
				volume = "engineOn*camPos*asphalt*(latSlipDrive Factor[0.1, 0.4])*(Speed Factor[2, 15])";
			};
			class turn_right_ext_road
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02",0.70794576,1,80};
				frequency = 1;
				volume = "engineOn*camPos*asphalt*(latSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
			};
			class breaking_ext_dirt
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_14_dirt_breaking",0.70794576,1,60};
				frequency = 1;
				volume = "engineOn*camPos*(1-asphalt)*(LongSlipDrive Factor[-0.1, -0.4])*(Speed Factor[1, 15])";
			};
			class acceleration_ext_dirt
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_16_dirt_acceleration",0.70794576,1,60};
				frequency = 1;
				volume = "engineOn*camPos*(1-asphalt)*(LongSlipDrive Factor[0.1, 0.4])*(Speed Factor[15, 1])";
			};
			class turn_left_ext_dirt
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt",0.70794576,1,60};
				frequency = 1;
				volume = "engineOn*camPos*(1-asphalt)*(latSlipDrive Factor[0.1, 0.4])*(Speed Factor[1, 15])";
			};
			class turn_right_ext_dirt
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt",0.70794576,1,60};
				frequency = 1;
				volume = "engineOn*camPos*(1-asphalt)*(latSlipDrive Factor[-0.1, -0.4])*(Speed Factor[1, 15])";
			};
			class breaking_int_road
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_04_int",0.31622776,1};
				frequency = 1;
				volume = "engineOn*asphalt*(1-camPos)*(LongSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
			};
			class acceleration_int_road
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int",0.31622776,1};
				frequency = 1;
				volume = "engineOn*asphalt*(1-camPos)*(LongSlipDrive Factor[0.1, 0.4])*(Speed Factor[15, 2])";
			};
			class turn_left_int_road
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int",0.31622776,1};
				frequency = 1;
				volume = "engineOn*asphalt*(1-camPos)*(latSlipDrive Factor[0.1, 0.4])*(Speed Factor[2, 15])";
			};
			class turn_right_int_road
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_loop_02_int",0.31622776,1};
				frequency = 1;
				volume = "engineOn*asphalt*(1-camPos)*(latSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
			};
			class breaking_int_dirt
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_14_dirt_breaking_int",0.31622776,1};
				frequency = 1;
				volume = "engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive Factor[-01, -0.4])*(Speed Factor[2, 15])";
			};
			class acceleration_int_dirt
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_16_dirt_acceleration_int",0.31622776,1};
				frequency = 1;
				volume = "engineOn*(1-asphalt)*(1-camPos)*(LongSlipDrive Factor[0.1, 0.4])*(Speed Factor[15, 2])";
			};
			class turn_left_int_dirt
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt_int",0.31622776,1};
				frequency = 1;
				volume = "engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive Factor[0.1, 0.4])*(Speed Factor[2, 15])";
			};
			class turn_right_int_dirt
			{
				sound[] = {"A3\Sounds_F\vehicles\soft\noises\slipping_tires_18_dirt_int",0.31622776,1};
				frequency = 1;
				volume = "engineOn*(1-asphalt)*(1-camPos)*(latSlipDrive Factor[-0.1, -0.4])*(Speed Factor[2, 15])";
			};
		};
		class RenderTargets
		{
			class LeftMirror
			{
				renderTarget = "rendertarget0";
				class CameraView1
				{
					pointPosition = "PIP0_pos";
					pointDirection = "PIP0_dir";
					renderQuality = 2;
					renderVisionMode = 0;
					fov = 0.7;
				};
			};
			class RearCam
			{
				renderTarget = "rendertarget1";
				class CameraView1
				{
					pointPosition = "PIP1_pos";
					pointDirection = "PIP1_dir";
					renderQuality = 2;
					renderVisionMode = 0;
					fov = 0.7;
				};
			};
			class FrontCam
			{
				renderTarget = "rendertarget2";
				class CameraView1
				{
					pointPosition = "PIP2_pos";
					pointDirection = "PIP2_dir";
					renderQuality = 2;
					renderVisionMode = 0;
					fov = 0.7;
				};
			};
		};
		thrustDelay = 0.2;
		brakeIdleSpeed = 1.78;
		maxSpeed = 210;
		fuelCapacity = 120;
		wheelCircumference = 2.519;
		antiRollbarForceCoef = 10;
		antiRollbarForceLimit = 10;
		antiRollbarSpeedMin = 20;
		antiRollbarSpeedMax = 40;
		class complexGearbox
		{
			GearboxRatios[] = {"R1",-2.575,"N",0,"D1",4.5,"D2",2.6,"D3",1.32,"D4",0.96,"D5",0.65};
			TransmissionRatios[] = {"High",5.539};
			gearBoxMode = "auto";
			moveOffGear = 1;
			driveString = "D";
			neutralString = "N";
			reverseString = "R";
		};
		simulation = "carx";
		dampersBumpCoef = 6;
		differentialType = "all_limited";
		frontRearSplit = 0.5;
		frontBias = 1.3;
		centreBias = 1.3;
		clutchStrength = 55;
		enginePower = 320;
		maxOmega = 471;
		peakTorque = 853;
		idleRpm = 800;
		redRpm = 4500;
		dampingRateFullThrottle = 0.08;
		dampingRateZeroThrottleClutchEngaged = 0.35;
		dampingRateZeroThrottleClutchDisengaged = 0.35;
		torqueCurve[] = {{ 0,0 },{ 0.14,0.41 },{ 0.29,0.77 },{ 0.43,1 },{ 0.57,1 },{ 0.71,0.87 },{ 0.86,0.77 },{ 1,0.62 }};
		changeGearMinEffectivity[] = {0.95,0.15,0.95,0.95,0.95,0.95,0.95};
		switchTime = 0.31;
		latency = 1;
		class Wheels
		{
			class LF
			{
				boneName = "wheel_1_1_damper";
				steering = 1;
				side = "left";
				center = "wheel_1_1_axis";
				boundary = "wheel_1_1_bound";
				mass = 20;
				MOI = 3.3;
				dampingRate = 0.5;
				maxBrakeTorque = 5500;
				maxHandBrakeTorque = 0;
				suspTravelDirection[] = {0,-1,0};
				suspForceAppPointOffset = "wheel_1_1_axis";
				tireForceAppPointOffset = "wheel_1_1_axis";
				maxCompression = 0.15;
				mMaxDroop = 0.15;
				sprungMass = 550;
				springStrength = 90875;
				springDamperRate = 20740;
				longitudinalStiffnessPerUnitGravity = 8700;
				latStiffX = 35;
				latStiffY = 180;
				frictionVsSlipGraph[] = {{ 0.17,0.85 },{ 0.4,0.65 },{ 1,0.4 }};
			};
			class LR: LF
			{
				boneName = "wheel_1_2_damper";
				steering = 0;
				center = "wheel_1_2_axis";
				boundary = "wheel_1_2_bound";
				suspForceAppPointOffset = "wheel_1_2_axis";
				tireForceAppPointOffset = "wheel_1_2_axis";
				maxHandBrakeTorque = 4500;
				latStiffY = 180;
				sprungMass = 550;
				springStrength = 90875;
				springDamperRate = 20740;
			};
			class RF: LF
			{
				boneName = "wheel_2_1_damper";
				center = "wheel_2_1_axis";
				boundary = "wheel_2_1_bound";
				suspForceAppPointOffset = "wheel_2_1_axis";
				tireForceAppPointOffset = "wheel_2_1_axis";
				steering = 1;
				side = "right";
			};
			class RR: RF
			{
				boneName = "wheel_2_2_damper";
				steering = 0;
				center = "wheel_2_2_axis";
				boundary = "wheel_2_2_bound";
				suspForceAppPointOffset = "wheel_2_2_axis";
				tireForceAppPointOffset = "wheel_2_2_axis";
				maxHandBrakeTorque = 4500;
				latStiffY = 180;
				sprungMass = 550;
				springStrength = 90875;
				springDamperRate = 20740;
			};
		};
		class Damage
		{
			tex[] = {};
			mat[] = {"A3\soft_F\Quadbike_01\Data\Quadbike_01_base.rvmat","A3\soft_F\Quadbike_01\Data\Quadbike_01_base_damage.rvmat","A3\soft_F\Quadbike_01\Data\Quadbike_01_base_destruct.rvmat"};
		};
		class Reflectors
		{
			class LightCarHeadL01
			{
				color[] = {1900,1800,1700};
				ambient[] = {5,5,5};
				position = "LightCarHeadL01";
				direction = "LightCarHeadL01_end";
				hitpoint = "Light_L";
				selection = "Light_L";
				size = 1;
				innerAngle = 100;
				outerAngle = 179;
				coneFadeCoef = 10;
				intensity = 1;
				useFlare = 1;
				dayLight = 0;
				flareSize = 1;
				class Attenuation
				{
					start = 1;
					constant = 0;
					linear = 0;
					quadratic = 0.25;
					hardLimitStart = 30;
					hardLimitEnd = 60;
				};
			};
			class LightCarHeadL02: LightCarHeadL01
			{
				position = "LightCarHeadL02";
				direction = "LightCarHeadL02_end";
				FlareSize = 0.5;
			};
			class LightCarHeadR01: LightCarHeadL01
			{
				position = "LightCarHeadR01";
				direction = "LightCarHeadR01_end";
				hitpoint = "Light_R";
				selection = "Light_R";
			};
			class LightCarHeadR02: LightCarHeadR01
			{
				position = "LightCarHeadR02";
				direction = "LightCarHeadR02_end";
				FlareSize = 0.5;
			};
		};
		aggregateReflectors[] = {{ "LightCarHeadL01","LightCarHeadL02" },{ "LightCarHeadR01","LightCarHeadR02" }};
	};
	class chausette_TahoePoliceELS: DAR_TahoePolBase
	{
		author = "Ported by RichardsD, from BxBugs123 and modif by chausette";
		displayname = "Chevrolet Tahoe PPV ELs";
		model = "DAR_TahoePPV\DAR_TahoePPV.p3d";
		scope = 2;
		side = "TCivilian";
		faction = "CIV_F";
		crew = "C_man_1";
		typicalCargo[] = {"C_man_1"};
		hiddenSelections[] = {"jump","lb-left-front-corner","lb-left-takedown","lb-front-blue-1","lb-front-blue-2","lb-front-red-2","lb-front-red-1","lb-right-takedown","lb-right-front-corner","lb-right-back-corner","lb-back-red-1","lb-back-red-2","lb-back-red-3","lb-back-blue-3","lb-back-blue-2","lb-back-blue-1","lb-left-back-corner","lb-back-yellow-1","lb-back-yellow-2","lb-back-yellow-3","lb-back-yellow-4","lb-back-yellow-5","lb-back-yellow-6","lb-left-alley","lb-right-alley","lb-ion-blue","lb-ion-red","radar_patrol_c","radar_patrol_d","radar_patrol_u","radar_fast_c","radar_fast_d","radar_fast_u","radar_target_c","radar_target_d","radar_target_u"};
		hiddenSelectionsTextures[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
		class eventhandlers
		{
			init = "_this execVM '\DAR_TahoePPV\PoliceData\Scripts\init.sqf';";
		};
		class UserActions
		{
			class codeOne
			{
				displayName = "<t color='#0000ff'>ELS</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player";
				statement = "createDialog ""ELS"";";
				onlyForplayer = 0;
			};
			
			class takedownoff
			{
				displayName = "<t color='#ffffff'>Takedowns off</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_takedown' != 0)";
				statement = "this animate ['ani_takedown', 0]";
				onlyForplayer = 0;
			};
			class takedown3
			{
				displayName = "<t color='#ffffff'>Takedowns ON</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_takedown' != 0.3)";
				statement = "this animate ['ani_takedown', 0.3]";
				onlyForplayer = 0;
			};
		};
		class AnimationSources: AnimationSources
		{
			class Zeroanimation
			{
				source = "user";
				animPeriod = 0;
				initPhase = 0;
			};
			class LightAnim
			{
				source = "user";
				animPeriod = 1;
				initPhase = 1;
			};
			class ani_lightbar
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_siren
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_alley
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_takedown
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_directional
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_radar
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
		};
	};
	class DAR_TahoePoliceSlick: DAR_TahoePolBase
	{
		author = "Ported by RichardsD, from BxBugs123";
		displayname = "2007 Chevrolet Tahoe Slicktop PPV";
		model = "DAR_TahoePPV\DAR_TahoePPVS.p3d";
		scope = 2;
		side = "TCivilian";
		faction = "CIV_F";
		crew = "C_man_1";
		typicalCargo[] = {"C_man_1"};
		hiddenSelections[] = {"jump","lb-left-front-corner","lb-left-takedown","lb-front-blue-1","lb-front-blue-2","lb-front-red-2","lb-front-red-1","lb-right-takedown","lb-right-front-corner","lb-right-back-corner","lb-back-red-1","lb-back-red-2","lb-back-red-3","lb-back-blue-3","lb-back-blue-2","lb-back-blue-1","lb-left-back-corner","lb-back-yellow-1","lb-back-yellow-2","lb-back-yellow-3","lb-back-yellow-4","lb-back-yellow-5","lb-back-yellow-6","lb-left-alley","lb-right-alley","lb-ion-blue","lb-ion-red","radar_patrol_c","radar_patrol_d","radar_patrol_u","radar_fast_c","radar_fast_d","radar_fast_u","radar_target_c","radar_target_d","radar_target_u"};
		hiddenSelectionsTextures[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
		class eventhandlers
		{
			init = "_this execVM '\DAR_TahoePPV\PoliceData\Scripts\init.sqf';";
		};
		class UserActions
		{
			class codeOne
			{
				displayName = "<t color='#0000ff'>Code One</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_lightbar' != 0)";
				statement = "this animate ['ani_lightbar', 0], this animate ['ani_siren', 0], this animate ['ani_directional', 0]";
				onlyForplayer = 0;
			};
			class LightMode1
			{
				displayName = "<t color='#0000ff'>Code Two</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_lightbar' != 0.1)";
				statement = "this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class LightMode2
			{
				displayName = "<t color='#0000ff'>Code Two</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' > 0)";
				statement = "this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2], this animate ['ani_siren', 0]";
				onlyForplayer = 0;
			};
			class sirenon
			{
				displayName = "<t color='#ff0000'>Code Three</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0)";
				statement = "this animate ['ani_siren', 0.2], this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class togglesiren1
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.2)";
				statement = "this animate ['ani_siren', 0.4]";
				onlyForplayer = 0;
			};
			class togglesiren2
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.4)";
				statement = "this animate ['ani_siren', 0.6]";
				onlyForplayer = 0;
			};
			class togglesiren3
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.6)";
				statement = "this animate ['ani_siren', 0.2]";
				onlyForplayer = 0;
			};
			class directionaloff
			{
				displayName = "<t color='#ffff00'>Directional OFF</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0)";
				statement = "this animate ['ani_directional', 0]";
				onlyForplayer = 0;
			};
			class directional1
			{
				displayName = "<t color='#ffff00'>Directional Warning</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.1)";
				statement = "this animate ['ani_directional', 0.1]";
				onlyForplayer = 0;
			};
			class directional2
			{
				displayName = "<t color='#ffff00'>Directional Flashing</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.2)";
				statement = "this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class directional3
			{
				displayName = "<t color='#ffff00'>Directional Right</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.3)";
				statement = "this animate ['ani_directional', 0.3]";
				onlyForplayer = 0;
			};
			class directional4
			{
				displayName = "<t color='#ffff00'>Directional Left</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.4)";
				statement = "this animate ['ani_directional', 0.4]";
				onlyForplayer = 0;
			};
			class directional5
			{
				displayName = "<t color='#ffff00'>Directional Both</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.5)";
				statement = "this animate ['ani_directional', 0.5]";
				onlyForplayer = 0;
			};
			class takedownoff
			{
				displayName = "<t color='#ffffff'>Takedowns off</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_takedown' != 0)";
				statement = "this animate ['ani_takedown', 0]";
				onlyForplayer = 0;
			};
			class takedown3
			{
				displayName = "<t color='#ffffff'>Takedowns ON</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_takedown' != 0.3)";
				statement = "this animate ['ani_takedown', 0.3]";
				onlyForplayer = 0;
			};
			class radaron
			{
				displayName = "<t color='#000000'>Radar ON</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_radar' == 0)";
				statement = "this animate ['ani_radar', 0.3]";
				onlyForplayer = 0;
			};
			class radaroff: radaron
			{
				displayName = "<t color='#000000'>Radar OFF</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_radar' != 0)";
				statement = "this animate ['ani_radar', 0]";
				onlyForplayer = 0;
			};
		};
		class AnimationSources: AnimationSources
		{
			class Zeroanimation
			{
				source = "user";
				animPeriod = 0;
				initPhase = 0;
			};
			class LightAnim
			{
				source = "user";
				animPeriod = 1;
				initPhase = 1;
			};
			class ani_lightbar
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_siren
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_alley
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_takedown
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_directional
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_radar
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
		};
	};
	class DAR_TahoePoliceDet: DAR_TahoePolBase
	{
		author = "Ported by RichardsD, from BxBugs123";
		displayname = "2007 Chevrolet Tahoe Stealth PPV";
		model = "DAR_TahoePPV\DAR_TahoePPVD.p3d";
		scope = 2;
		side = "TCivilian";
		faction = "CIV_F";
		crew = "C_man_1";
		typicalCargo[] = {"C_man_1"};
		hiddenSelections[] = {"jump","lb-left-front-corner","lb-left-takedown","lb-front-blue-1","lb-front-blue-2","lb-front-red-2","lb-front-red-1","lb-right-takedown","lb-right-front-corner","lb-right-back-corner","lb-back-red-1","lb-back-red-2","lb-back-red-3","lb-back-blue-3","lb-back-blue-2","lb-back-blue-1","lb-left-back-corner","lb-back-yellow-1","lb-back-yellow-2","lb-back-yellow-3","lb-back-yellow-4","lb-back-yellow-5","lb-back-yellow-6","lb-left-alley","lb-right-alley","lb-ion-blue","lb-ion-red","radar_patrol_c","radar_patrol_d","radar_patrol_u","radar_fast_c","radar_fast_d","radar_fast_u","radar_target_c","radar_target_d","radar_target_u"};
		hiddenSelectionsTextures[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
		class eventhandlers
		{
			init = "_this execVM '\DAR_TahoePPV\PoliceData\Scripts\init.sqf';";
		};
		class UserActions
		{
			class codeOne
			{
				displayName = "<t color='#0000ff'>Code One</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_lightbar' != 0)";
				statement = "this animate ['ani_lightbar', 0], this animate ['ani_siren', 0], this animate ['ani_directional', 0]";
				onlyForplayer = 0;
			};
			class LightMode1
			{
				displayName = "<t color='#0000ff'>Code Two</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_lightbar' != 0.1)";
				statement = "this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class LightMode2
			{
				displayName = "<t color='#0000ff'>Code Two</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' > 0)";
				statement = "this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2], this animate ['ani_siren', 0]";
				onlyForplayer = 0;
			};
			class sirenon
			{
				displayName = "<t color='#ff0000'>Code Three</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0)";
				statement = "this animate ['ani_siren', 0.2], this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class togglesiren1
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.2)";
				statement = "this animate ['ani_siren', 0.4]";
				onlyForplayer = 0;
			};
			class togglesiren2
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.4)";
				statement = "this animate ['ani_siren', 0.6]";
				onlyForplayer = 0;
			};
			class togglesiren3
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.6)";
				statement = "this animate ['ani_siren', 0.2]";
				onlyForplayer = 0;
			};
			class directionaloff
			{
				displayName = "<t color='#ffff00'>Directional OFF</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0)";
				statement = "this animate ['ani_directional', 0]";
				onlyForplayer = 0;
			};
			class directional1
			{
				displayName = "<t color='#ffff00'>Directional Warning</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.1)";
				statement = "this animate ['ani_directional', 0.1]";
				onlyForplayer = 0;
			};
			class directional2
			{
				displayName = "<t color='#ffff00'>Directional Flashing</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.2)";
				statement = "this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class directional3
			{
				displayName = "<t color='#ffff00'>Directional Right</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.3)";
				statement = "this animate ['ani_directional', 0.3]";
				onlyForplayer = 0;
			};
			class directional4
			{
				displayName = "<t color='#ffff00'>Directional Left</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.4)";
				statement = "this animate ['ani_directional', 0.4]";
				onlyForplayer = 0;
			};
			class directional5
			{
				displayName = "<t color='#ffff00'>Directional Both</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.5)";
				statement = "this animate ['ani_directional', 0.5]";
				onlyForplayer = 0;
			};
			class takedownoff
			{
				displayName = "<t color='#ffffff'>Takedowns off</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_takedown' != 0)";
				statement = "this animate ['ani_takedown', 0]";
				onlyForplayer = 0;
			};
			class takedown3
			{
				displayName = "<t color='#ffffff'>Takedowns ON</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_takedown' != 0.3)";
				statement = "this animate ['ani_takedown', 0.3]";
				onlyForplayer = 0;
			};
			class radaron
			{
				displayName = "<t color='#000000'>Radar ON</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_radar' == 0)";
				statement = "this animate ['ani_radar', 0.3]";
				onlyForplayer = 0;
			};
			class radaroff: radaron
			{
				displayName = "<t color='#000000'>Radar OFF</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_radar' != 0)";
				statement = "this animate ['ani_radar', 0]";
				onlyForplayer = 0;
			};
		};
		class AnimationSources: AnimationSources
		{
			class Zeroanimation
			{
				source = "user";
				animPeriod = 0;
				initPhase = 0;
			};
			class LightAnim
			{
				source = "user";
				animPeriod = 1;
				initPhase = 1;
			};
			class ani_lightbar
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_siren
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_alley
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_takedown
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_directional
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_radar
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
		};
	};
	class DAR_TahoePolice: DAR_TahoePolBase
	{
		author = "Ported by RichardsD, from BxBugs123";
		displayname = "2007 Chevrolet Tahoe PPV";
		model = "DAR_TahoePPV\DAR_TahoePPV.p3d";
		scope = 2;
		side = "TCivilian";
		faction = "CIV_F";
		crew = "C_man_1";
		typicalCargo[] = {"C_man_1"};
		hiddenSelections[] = {"jump","lb-left-front-corner","lb-left-takedown","lb-front-blue-1","lb-front-blue-2","lb-front-red-2","lb-front-red-1","lb-right-takedown","lb-right-front-corner","lb-right-back-corner","lb-back-red-1","lb-back-red-2","lb-back-red-3","lb-back-blue-3","lb-back-blue-2","lb-back-blue-1","lb-left-back-corner","lb-back-yellow-1","lb-back-yellow-2","lb-back-yellow-3","lb-back-yellow-4","lb-back-yellow-5","lb-back-yellow-6","lb-left-alley","lb-right-alley","lb-ion-blue","lb-ion-red","radar_patrol_c","radar_patrol_d","radar_patrol_u","radar_fast_c","radar_fast_d","radar_fast_u","radar_target_c","radar_target_d","radar_target_u"};
		hiddenSelectionsTextures[] = {"","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","","",""};
		class eventhandlers
		{
			init = "_this execVM '\DAR_TahoePPV\PoliceData\Scripts\init.sqf';";
		};
		class UserActions
		{
			class codeOne
			{
				displayName = "<t color='#0000ff'>Code One</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_lightbar' != 0)";
				statement = "this animate ['ani_lightbar', 0], this animate ['ani_siren', 0], this animate ['ani_directional', 0]";
				onlyForplayer = 0;
			};
			class LightMode1
			{
				displayName = "<t color='#0000ff'>Code Two</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_lightbar' != 0.1)";
				statement = "this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class LightMode2
			{
				displayName = "<t color='#0000ff'>Code Two</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' > 0)";
				statement = "this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2], this animate ['ani_siren', 0]";
				onlyForplayer = 0;
			};
			class sirenon
			{
				displayName = "<t color='#ff0000'>Code Three</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0)";
				statement = "this animate ['ani_siren', 0.2], this animate ['ani_lightbar', 0.1], this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class togglesiren1
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.2)";
				statement = "this animate ['ani_siren', 0.4]";
				onlyForplayer = 0;
			};
			class togglesiren2
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.4)";
				statement = "this animate ['ani_siren', 0.6]";
				onlyForplayer = 0;
			};
			class togglesiren3
			{
				displayName = "<t color='#ff0000'>Toggle Siren</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_siren' == 0.6)";
				statement = "this animate ['ani_siren', 0.2]";
				onlyForplayer = 0;
			};
			class directionaloff
			{
				displayName = "<t color='#ffff00'>Directional OFF</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0)";
				statement = "this animate ['ani_directional', 0]";
				onlyForplayer = 0;
			};
			class directional1
			{
				displayName = "<t color='#ffff00'>Directional Warning</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.1)";
				statement = "this animate ['ani_directional', 0.1]";
				onlyForplayer = 0;
			};
			class directional2
			{
				displayName = "<t color='#ffff00'>Directional Flashing</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.2)";
				statement = "this animate ['ani_directional', 0.2]";
				onlyForplayer = 0;
			};
			class directional3
			{
				displayName = "<t color='#ffff00'>Directional Right</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.3)";
				statement = "this animate ['ani_directional', 0.3]";
				onlyForplayer = 0;
			};
			class directional4
			{
				displayName = "<t color='#ffff00'>Directional Left</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.4)";
				statement = "this animate ['ani_directional', 0.4]";
				onlyForplayer = 0;
			};
			class directional5
			{
				displayName = "<t color='#ffff00'>Directional Both</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_directional' != 0.5)";
				statement = "this animate ['ani_directional', 0.5]";
				onlyForplayer = 0;
			};
			class takedownoff
			{
				displayName = "<t color='#ffffff'>Takedowns off</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_takedown' != 0)";
				statement = "this animate ['ani_takedown', 0]";
				onlyForplayer = 0;
			};
			class takedown3
			{
				displayName = "<t color='#ffffff'>Takedowns ON</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_takedown' != 0.3)";
				statement = "this animate ['ani_takedown', 0.3]";
				onlyForplayer = 0;
			};
			class radaron
			{
				displayName = "<t color='#000000'>Radar ON</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_radar' == 0)";
				statement = "this animate ['ani_radar', 0.3]";
				onlyForplayer = 0;
			};
			class radaroff: radaron
			{
				displayName = "<t color='#000000'>Radar OFF</t>";
				position = "drivewheel";
				radius = 1000;
				condition = "driver this == player && (this animationPhase 'ani_radar' != 0)";
				statement = "this animate ['ani_radar', 0]";
				onlyForplayer = 0;
			};
		};
		class AnimationSources: AnimationSources
		{
			class Zeroanimation
			{
				source = "user";
				animPeriod = 0;
				initPhase = 0;
			};
			class LightAnim
			{
				source = "user";
				animPeriod = 1;
				initPhase = 1;
			};
			class ani_lightbar
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_siren
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_alley
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_takedown
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_directional
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
			class ani_radar
			{
				source = "user";
				animPeriod = 1;
				initPhase = 0;
			};
		};
	};
};
//};