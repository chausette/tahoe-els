//addAction [ELS, { createDialog ELS; }];

private[_car,_nearVehicles,_className,_action];
createDialog ELS;
disableSerialization;
if ( vehicle player == player ) then { hint vous devez être dans votre voiture; };
_action = _this select 0;
_nearVehicles = nearestObject [player,Car];
_className = typeOf _nearVehicles;
_car = getText(configFile  CfgVehicles  _className  displayName);
switch (_action) do 
	{
		case (0) 
		{
			_nearVehicles animate ['ani_lightbar', 0];
			hint Gyrophare eteint;

		};
		case (1) 
		{
			_nearVehicles animate ['ani_lightbar', 0.1];
			hint Gyrophare mode 1;
		};
		case (2) 
		{
			hint Gyrophare mode 2 not set;
		};
		case (3) 
		{
			hint Gyrophare mode 3 not set;
		};
		case (4) 
		{
			hint Gyrophare mode 4 not set;
		};
		bare de direction
		case (5) 
		{
			hint barre de directon eteinte;
			_nearVehicles animate['ani_directional', 0]
		};
		case (6) 
		{
			hint barre de directon gauche;
			_nearVehicles animate['ani_directional', 0.4]
		};
		case (7) 
		{
			hint barre de directon double;
			_nearVehicles animate['ani_directional', 0.5]
		};
		case (8) 
		{
			hint barre de directon droite;
			_nearVehicles animate['ani_directional', 0.3]
		};
		case (9) 
		{
			hint barre de directon flash;
			_nearVehicles animate['ani_directional', 0.2]
		};
		sirene
		case (10) 
		{
			_nearVehicles animate['ani_siren', 0.0]
		};
		case (11) 
		{
			_nearVehicles animate['ani_siren', 0.2]
			_nearVehicles animate ['ani_lightbar', 0.1];
		};
		case (12) 
		{
			_nearVehicles animate['ani_siren', 0.4]
			_nearVehicles animate ['ani_lightbar', 0.1];
		};
	}; END SWITCH