/*
 Created by Innocent Bystander for TCG's Island Life, adapted from best_radar.sqf
 TODO:  Find the speedlimit the target car is in and display the hint based off that (instead of always > 50)
*/

private["_veh","_radardir","_Beam","_Beam2","_tag","_tag2","_dbest","_tagbest","_getspeed1","_dfast","_tagfast","_getspeed2","_best1","_best2","_best3","_fast1","_fast2","_fast3","_besttext1","_besttext2","_besttext3","_fasttext1","_fasttext2","_fasttext3","_speederid","_speederspeed"];
_veh = _this;
_radardir ="";
while {alive _veh} do {
	if (!IsNull driver _veh && driver _veh == player && _veh animationPhase "ani_radar" > 0) then {
		if (_veh animationPhase "ani_radar" > 0.5) then 
		{
			_Beam = "#lightpoint" createVehicleLocal getpos _veh;
			_Beam lightAttachObject [ _veh, [ 0.0, 60.0, 0.75 ] ];
			_Beam setLightBrightness 0.0001;
			_Beam setLightAmbient[ 1.0, 0, 0 ];
			_Beam setLightColor[ 1.0, 0, 0 ];
			_Beam2 = "#lightpoint" createVehicleLocal getpos _veh;
			_Beam2 lightAttachObject [ _veh, [ 0.0, 200.0, 0.75 ] ];
			_Beam2 setLightBrightness 0.0001;
			_Beam2 setLightAmbient[ 1.0, 0, 0 ];
			_Beam2 setLightColor[ 1.0, 0, 0 ];
			_radardir = "Front";
		};
		if (_veh animationPhase "ani_radar" < 0.6 && _radardir != "Rear") then
		{
			_Beam = "#lightpoint" createVehicleLocal getpos _veh;
			_Beam lightAttachObject [ _veh, [ 0.0, 60.0, 0.75 ] ];
			_Beam setLightBrightness 0.0001;
			_Beam setLightAmbient[ 1.0, 0, 0 ];
			_Beam setLightColor[ 1.0, 0, 0 ];
			_Beam2 = "#lightpoint" createVehicleLocal getpos _veh;
			_Beam2 lightAttachObject [ _veh, [ 0.0, 200.0, 0.75 ] ];
			_Beam2 setLightBrightness 0.0001;
			_Beam2 setLightAmbient[ 1.0, 0, 0 ];
			_Beam2 setLightColor[ 1.0, 0, 0 ];
			_radardir = "Rear";
		};
		_tag2 = nearestObject [_Beam2, "LandVehicle"];
		_tag = nearestObject [_Beam, "LandVehicle"];
		if (driver _tag == driver _veh || isNull driver _tag) then
		{
			_veh setobjecttexture [34,"\DAR_ImpalaPI\gdigits\g0.pac"];
			_veh setobjecttexture [33,"\DAR_ImpalaPI\gdigits\g0.pac"];
			_veh setobjecttexture [32,"\DAR_ImpalaPI\gdigits\g0.pac"];
			_veh setobjecttexture [31,"\DAR_ImpalaPI\gdigits\g0.pac"]; 
			_veh setobjecttexture [30,"\DAR_ImpalaPI\gdigits\g0.pac"]; 
			_veh setobjecttexture [29,"\DAR_ImpalaPI\gdigits\g0.pac"];
			_tagfast = 0;
			_tagbest = 0;
			_speederid = "";
			_speederspeed = 0;
			_best1 = 0;
			_best2 = 0;
			_best3 = 0;
			_fast1 = 0;
			_fast2 = 0;
			_fast3 = 0;			
		} else {
			if(isNull _tag) then {_tag = _tag2};
			if(isNull _tag2) then {_tag2 = _tag};
			_dbest = driver _tag;
			_getspeed1 = speed _tag;
			_tagbest = ceil(_getspeed1);
			_dfast =  driver _tag2;
			_getspeed2 = speed _tag2;
			_tagfast = ceil(_getspeed2);
			if (_tagfast > _speederspeed) then {_speederspeed = _tagfast; _speederid = _dfast;}else{_tagfast = _speederspeed; _dfast = _speederid;};
			
			if (isNull _dbest) then {_dbest = "";_tagbest = 0};
			if (local driver _veh && _tagfast > 1) then
			{
				hintSilent format ["Top Speed: %2", _dbest, _tagfast, _radardir];
			};
			if (_tagbest < 1) then {_tagbest = 0;};
			if (_tagfast < 1) then {_tagfast = 0;};

			_best1 = 0;
			_best2 = 0;
			_best3 = _tagbest;
			_fast1 = 0;
			_fast2 = 0;
			_fast3 = _tagfast;

			while {_best3 > 9} do {_best3 = _best3 - 10; _best2 = _best2 + 1; if (_best2 > 9) then { _best1 = _best1 + 1; _best2 = 0}};
			while {_fast3 > 9} do {_fast3 = _fast3 - 10; _fast2 = _fast2 + 1; if (_fast2 > 9) then { _fast1 = _fast1 + 1; _fast2 = 0}};  
			_besttext1 = format ["\DAR_ImpalaPI\gdigits\g%1.pac", _best1]; 
			_besttext2 = format ["\DAR_ImpalaPI\gdigits\g%1.pac", _best2]; 
			_besttext3 = format ["\DAR_ImpalaPI\gdigits\g%1.pac", _best3]; 
			_veh setobjecttexture [34, _besttext1 ]; 
			_veh setobjecttexture [33, _besttext2 ]; 
			_veh setobjecttexture [32, _besttext3 ]; 
			_fasttext1 = format ["\DAR_ImpalaPI\gdigits\g%1.pac", _fast1]; 
			_fasttext2 = format ["\DAR_ImpalaPI\gdigits\g%1.pac", _fast2]; 
			_fasttext3 = format ["\DAR_ImpalaPI\gdigits\g%1.pac", _fast3];
			
			_veh setobjecttexture [31, _fasttext1 ]; 
			_veh setobjecttexture [30, _fasttext2 ]; 
			_veh setobjecttexture [29, _fasttext3 ];
		};
		sleep 0.5;
	} else {
		_veh setobjecttexture [34,""];
		_veh setobjecttexture [33,""];
		_veh setobjecttexture [32,""];
		_veh setobjecttexture [31,""];
		_veh setobjecttexture [30,""];
		_veh setobjecttexture [29,""];
		_speederid = "";
		_speederspeed = 0;	
		waitUntil {sleep 3; (!IsNull driver _veh && driver _veh == player && _veh animationPhase "ani_radar" > 0 && getdammage _veh < 0.7)};
	};
};
exit;